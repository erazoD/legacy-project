<?php
/**
 * legacy_theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package legacy_theme
 */




if ( ! function_exists( 'legacy_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function legacy_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on legacy_theme, use a find and replace
	 * to change 'legacy_theme' to the name of your theme in all the template files.
	 */




	load_theme_textdomain( 'legacy_theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'Holi' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'legacy_theme' ),
		) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'legacy_theme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
		) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'legacy_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function legacy_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'legacy_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'legacy_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function legacy_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'legacy_theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'legacy_theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
		) );
}
add_action( 'widgets_init', 'legacy_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function legacy_theme_scripts() {
	wp_enqueue_style( 'legacy_theme-style', get_stylesheet_uri() );
	
	wp_enqueue_style( 'legacy_theme-materialize', get_template_directory_uri() . '/css/materialize.min.css');
	wp_enqueue_style( 'legacy_theme-slick-style', get_template_directory_uri() . '/css/slick.css');
	wp_enqueue_style( 'legacy_theme-slick-theme', get_template_directory_uri() . '/css/slick-theme.css');
	/*wp_enqueue_style('legacy_theme_flex', get_template_directory_uri() . '/css/flexboxgrid.css');*/
	wp_enqueue_script( 'legacy_theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'legacy_theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_style('legacy_theme_estilo', get_template_directory_uri() . '/css/estilos.css');
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'legacy_theme_scripts' );

function legacy_theme_js(){
	wp_enqueue_script('legacy_theme_jquery', get_template_directory_uri() . '/js/jquery-2.2.4.js');
	wp_enqueue_script('legacy_theme_materialize_script', get_template_directory_uri() . '/js/materialize.min.js');
	wp_enqueue_script('legacy_theme_slick', get_template_directory_uri() . '/js/slick.js');
	wp_enqueue_script('legacy_theme_main', get_template_directory_uri() . '/js/main.js');

}

add_action('wp_footer', 'legacy_theme_js' );

function register_my_menu() {
  register_nav_menu('mobile-menu',__( 'Mobile Menu' ));
}
add_action( 'init', 'register_my_menu' );
