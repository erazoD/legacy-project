<?php
/**
 *  Mostrar menu parcial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package legacy_theme
 */
?>
<div class="navegador-somos row">
 <div class="container">
 <ul>
 	<li><a href="/legacy_stage/quienes-somos/" class="fuente-a">Quiénes somos</a></li>
 	<li><a href="/legacy_stage/nuestro-proposito/" class="fuente-a">Nuestro proposito</a></li>
 	<li><a href="/legacy_stage/nuestro-equipo/" class="fuente-a">Nuestro equipo</a></li>
 	<li><a href="/legacy_stage/imperativos-de-servicio/" class="fuente-a">Nuestro servicio</a></li>
 </ul>
 </div>
</div>
 

