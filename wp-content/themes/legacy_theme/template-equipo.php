<?php
/**
 * Template Name: Nuestro Equipo
 *
 * @package WordPress
 * @subpackage legacy_theme
 */

get_header(); ?>

<div id="secondary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="titulo-seccion">
			<div class="banner-quien col s12" style="background:url(<?php echo get_field('team_image_banner')['url'];?>);">
				<div class="info-texto">
					<h4 class='fuente-b'><?php echo get_field('team_text_banner');?></h4>
				</div>
			</div>   
		</div>
		<div class="secciones row">
			<div class="texto-equipo col s12">
				<div class="container">
					<p class='fuente-a'><?php echo get_field('team_text');?></p>
				</div>
			</div>
		</div>
		<?php get_template_part('menu-somos');?> 
	</main><!-- #main -->
</div><!-- #primary -->
<?php
// get_sidebar();
get_footer();
