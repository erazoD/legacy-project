$( document ).ready(function(){
    //var fondo_dos = ; 
    //var fondo_tres = ;

    $('.informa-reaccion').slick({
    	dots: false,
    	arrows: true,
    	infinite: true,
    	speed: 500,
    	autoplay: true,
    	autoplaySpeed: 4000,
    	slidesToShow: 1,
    	adaptiveHeight:true
    });

	//Desplegar la informacion de la solucion
	$('.titulo').click(function(){
		$(this).parent('.solucion').find('.contenido').slideToggle('slow');

	});
	// Añadir al menú la clase del nav
	$('#primary-menu').addClass('hide-on-med-and-down');

	// Añadir a los sub menus el id único
	$( ".sub-menu" ).each(function(index) {
		$(this).addClass( "dropdown-content" );
		$(this).attr('id', 'dropdown' + index);
	});

	// Identificar cada parent(que tiene sub-menus)
	$( "ul#primary-menu li.menu-item-has-children > a" ).each(function(index) {
		$(this).addClass('dropdown-button');
		$(this).attr('data-activates', 'dropdown' + index);
	});
	/*habilita el item colapsible del menu*/
	$(".dropdown-button").dropdown({
		hover: true
	});
	/*habilita el menu desplegable*/
	$(".button-collapse").sideNav();
	/*Suavizar el scroll dentro de la pagina*/
	var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
	   if(!isMobile) {
	var $root = $('html, body');
	$('.navega a').click(function() {
		$root.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 700);
		return false;
	});
	$('.footer-copyright a').click(function() {
		$root.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 700);
		return false;
	});
}
	/*segmentacion del menu interno para quienes somos*/
      	var nombre_path = window.location.pathname;
	$(".navegador-somos a").each(function(){
		if (nombre_path.indexOf($(this).attr('href')) != -1){

			$(this).parent('li').remove();
			console.log('llego hasta aqui');
		}
	});
	/*$(window).load(function(){ 
        var domina = document.domain;
        console.log('este es el dominio ' + domina);
    });*/

    

});

