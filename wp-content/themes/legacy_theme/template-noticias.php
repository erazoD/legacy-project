  
<?php
/**
 * Template Name: Noticias 
 *
 * @package WordPress
 * @subpackage legacy_theme
 */

get_header(); ?>

<div id="secondary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="titulo-seccion">
			<div class="banner-quien col s12" style="background:url(<?php echo get_field('news_banner')['url'];?>);">
				<div class="info-texto">
					<h4 class='fuente-b'><?php echo get_field('news_banner_text');?></h4>
				</div>
			</div>   
		</div>
		<div class="noticias row">
			<div class="container">
				<img class='logo-noticias' src="<?php echo get_field('news_logo')['url'];?>" alt="">
				<div class="linea"></div>
				<?php
				$params= array(
					'search' => false,
					'pagination' => false,
					'limit' => -1, 
					);
				$noticias = pods('noticia', $params); 
				$total_noticias= $noticias->total();
				if($total_noticias>0){
					while($noticias->fetch()){
						$imagen= $noticias->field('noticia_img.guid');
						$titulo= $noticias->field('noticia_titulo');
						$fecha = $noticias ->display('created');
						$newFecha = date("d-m-Y", strtotime($fecha));
						$resumen= $noticias->field('noticia_resumen');
						$fuente = $noticias->field('noticia_fuente');
						$link = $noticias->field('noticia_link');                        
						?>
						<div class="noticia-articulo col s12">
							<img src="<?php echo $imagen;?>" alt="noticia-thumbnail">
							<div class="info-noti">
								<h5 class='fuente-a'><?php echo $titulo?></h5>
								<h5 class="fecha fuente-a"><span class="bold">Publicado: </span><?php echo $newFecha;?></h5>
								<p class="summary fuente-a"><?php echo $resumen;?></p>
								<p class="fuente fuente-a"><span class="bold">Fuente: </span><?php echo $fuente;?></p>
								<div class="boton-full">
								<a href="<?php echo $link;?>" class="completa fuente-a">Ver noticia completa</a>
								</div>
							</div>
						</div>
					
					<?php 
				}
			}
			?>
		</div> 
		</div>
	</main><!-- #main -->
</div><!-- #primary -->
<?php
// get_sidebar();
get_footer();



