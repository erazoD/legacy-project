 <?php
/**
 * Template Name: Aliados
 *
 * @package WordPress
 * @subpackage legacy_theme
 */

get_header(); ?>

<div id="secondary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="aliados-logo">
		  <img src="<?php echo get_field('allies_logo')['url'];?>" alt="aliado">
		</div>
		<div class="secciones row">
			<div class="texto-aliado col s12">
				<div class="container">
				<h5 class='fuente-a'><?php echo get_field('allies_title');?></h5>
					<p class='fuente-a'><?php echo get_field('allies_text');?></p>
					<div class="boton-aliado">
					<a class='fuente-a' href="http://www.intlwealthprotection.com/home.asp"> Visite International Wealth Protection</a>
					</div>
				</div>
			</div>
		
		</div>
	</main><!-- #main -->
</div><!-- #primary -->
<?php
// get_sidebar();
get_footer();
