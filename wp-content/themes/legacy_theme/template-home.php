<?php
   /**
    * Template Name: Homepage
    *
    * @package WordPress
    * @subpackage legacy_theme
    {system:admin_email}
    */
  /*php $imagen_slider= get_field('slider_image_one');
  echo ' < div class="banner-home col s12" style="background:'. $imagen_slider['url'] .'">'; */

  get_header(); ?>
  <div id="primary" class="content-area">
   <main id="main" class="site-main" role="main">
    <div class="banner-home col s12" style="background:url(<?php echo get_field('slider_image_one')['url'];?>);">
     <div class="bienvenida">
       <h4 class="fuente-b"><?php $algo=get_field('slider_text_one'); echo $algo;?></h4>
     </div>  
   </div>
   <div class="nuestro-servicio row" id='nuestro-servicio'>
     <div class="container">
       <h4>NUESTRO SERVICIO</h4>
       <div class="servicio col s12 m4 l4">
         <img src="<?php bloginfo('template_url')?>/images/servicio-1.png" alt="">
         <h5>SU LEGADO EMPRESARIAL</h5>
         <p>Asesoramos en cómo proteger la
          continuidad de su empresa familiar
        </p>
      </div>
      <div class="servicio col s12 m4 l4">
       <img src="<?php bloginfo('template_url')?>/images/servicio-2.png" alt="">
       <h5>SU LEGADO PERSONAL</h5>
       <p>Asesoramos en soluciones de transferencia patrimonial para su familia</p>
     </div>
     <div class="servicio col s12 m4 l4">
       <img src="<?php bloginfo('template_url')?>/images/servicio-3.png" alt="">
       <h5>SU PATRIMONIO HOY</h5>
       <p>Asesoramos en soluciones creativas de planeación patrimonial en vida</p>
     </div>
   </div>
 </div><!--FIN SERVICIO-->

 <div class="informativo">
   <h4><?php echo get_field('titulo_area_one');?></h4>
   <div class="banner-info col s12" style="background:url(<?php echo get_field('slider_area_one')['url'];?>);">
    <div class="info-texto">
      <h4 class="fuente-b"><?php echo get_field('text_area_one');?></h4>
    </div>
  </div>   
  <div class="informa-reaccion">
   <div><h5>¿Qué impacto tendrá esto en su familia y en su calidad de vida?</h5></div>
   <div><h5>¿Tomarán la viuda, los hijos o un apoderado, participación activa en el negocio?</h5></div>
   <div><h5>¿Sus herederos tendrán los mismos intereses en la empresa o preferirán que les compren su participación?</h5></div>
   <div><h5>¿Cómo reaccionarán sus empleados, clientes, acreedores, bancos y proveedores?</h5></div>
 </div>
 <div class="informativo-dato">
  <img src="<?php bloginfo('template_url')?>/images/servicio-1l.png" alt="templo">
  <p> <?php echo get_field('info_area_one');?></p>
</div>
</div>

<div class="soluciones row">
  <div class="container">
   <h4>SOLUCIONES PARA SU LEGADO EMPRESARIAL</h4>
   <?php 
   $params= array(
    'where' =>"category.name='Empresarial'",
    'search' => false,
    'pagination' => false,
    'limit' => -1, 
    );
   $solutions = pods('solucion', $params); 
   $total_solutions= $solutions->total();
   if($total_solutions>0){
    while($solutions->fetch()){
      $titulo= $solutions->field('solution_title');
      $contenido= $solutions->field('solution_content');
      ?> 
      <div class="solucion col s12 m6 l6">
        <div class="titulo"><h5><?php echo $titulo;?></h5></div>
        <div class="contenido">
          <p><?php echo $contenido;?></p>
        </div>
      </div>
      <?php
    }
  }
  ?>
</div> 
</div> <!--FIN DE LEGADO EMPRESARIAL-->
<div class="informativo">
 <h4><?php echo get_field('titulo_area_two');?></h4>
 <div class="banner-info-2 col s12" style="background:url(<?php echo get_field('slider_area_two')['url'];?>);">
  <div class="info-texto">
    <h4 class="fuente-b"><?php echo get_field('text_area_two');?></h4>
  </div>
</div>   
<div class="informa-reaccion">
 <div><h5>¿Su familia continuará manteniendo el mismo estilo de vida?</h5></div>
 <div><h5>¿Se familia podrá la carga fiscal de patrimonio heredado?</h5></div>
 <div><h5>¿Qué deudas deja usted?</h5></div>
 <div><h5>¿Le interesaría dejar un legado social sin afectar el patrimonio de su familia?</h5></div>
</div>
<div class="informativo-dato">
  <img src="<?php bloginfo('template_url')?>/images/servicio-2l.png" alt="templo">
  <p><?php echo get_field('info_area_two');?></p>
</div>
</div> 
<div class="soluciones row">
  <div class="container">
    <h4>SOLUCIONES PARA SU LEGADO PERSONAL</h4>
    <?php 
    $params= array(
      'where' =>"category.name='Personal'",
      'search' => false,
      'pagination' => false,
      'limit' => -1, 
      );
    $solutions = pods('solucion', $params); 
    $total_solutions= $solutions->total();
    if($total_solutions>0){
      while($solutions->fetch()){
        $titulo= $solutions->display('name');
        $contenido= $solutions->field('solution_content');
        ?> 
        <div class="solucion col s12 m6 l6">
          <div class="titulo"><h5>
            <?php echo $titulo;?>
          </h5>
        </div>
        <div class="contenido">
          <p><?php echo $contenido;?></p>
        </div>
      </div>
      <?php
    }
  }
  ?>
</div>
</div><!--FIN DE LEGADO PERSONAL-->
<div class="informativo">
 <h4><?php echo get_field('titulo_area_three');?></h4>
 <div class="banner-info-3 col s12" style="background:url(<?php echo get_field('slider_image_three')['url'];?>);">
  <div class="info-texto">
    <h4 class="fuente-b"><?php echo get_field('text_area_three');?></h4>
  </div>
</div>   
<div class="informa-reaccion">
 <div><h5>¿Sabía que existen herramientas de protección patrimonial que tienen beneficios fiscales?</h5></div>
 <div><h5>¿Le gustaría beneficiase de un activo ilíquido sin afectar su legado patrimonial?</h5></div>
</div>
<div class="informativo-dato">
  <img src="<?php bloginfo('template_url')?>/images/servicio-3l.png" alt="templo">
  <p><?php echo get_field('info_area_three');?></p>
</div>
</div> 
<div class="soluciones row fondo">
  <div class="container">
   <h4>Soluciones para su patrimonio en vida</h4>

   <?php 
   $params= array(
    'where' =>"category.name='Patrimonio'",
    'search' => false,
    'pagination' => false,
    'limit' => -1, 
    );
   $solutions = pods('solucion', $params); 
   $total_solutions= $solutions->total();
   if($total_solutions>0){
    while($solutions->fetch()){
      $titulo= $solutions->field('solution_title');
      $contenido= $solutions->field('solution_content');
      ?> 
      <div class="solucion col s12 m6 l6">
        <div class="titulo"><h5><?php echo $titulo;?></h5></div>
        <div class="contenido">
          <p><?php echo $contenido;?></p>
        </div>
      </div>
      <?php
    }
  }
  ?>
</div>
</div><!--FIN DE SU PATRIMONIO HOY-->


</div> 
</main>
<!-- #main -->
</div>
<!-- #primary -->
<?php
// get_sidebar();
get_footer();