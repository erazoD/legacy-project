<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package legacy_theme
 */

?>
</div><!-- #content -->
<footer class="page-footer black legacy-footer" id='contactanos'>
	<div class="container">
		<div class="row f-contacto">
			<div class="col l6  s12">
				<h5 class="white-text">Hablemos ahora <br> <span class="sub-titulo">Tenemos mucho que contarle...</span></h5>
				<form class="col s12">
				
				<?php echo  do_shortcode('[ninja_form id=2]');?>

				</form>
			
			</div>
			<div class="col l4 offset-l2 s12 contacto">
				<h5 class="white-text">Contacto <br> <span class="sub-titulo">Legacy, <br> Wealth Protection LLC.</span></h5>
				<div class="linea"></div>
				<p class="white-text text-lighten-4">Dirección EE.UU: 8050 SW 72nd Ave Suite 2405 Miami, FL 33143 </p>
				<p class="white-text text-lighten-4">Correo: info@legacywp.us</p>
				<p class="white-text text-lighten-4">Teléfono Colombia: (57)(2) 373 3939</p>
				<p class="white-text text-lighten-4">Teléfono EE.UU: (1)(305) 381 2373</p>
			</div>
		</div>
	</div>
	<div class="footer-copyright white">
		<div class="container">
		    <img src="<?php bloginfo('template_url')?>/images/legacy-black.png" alt="Legacy">
		    <p>© LEGACY WP LLC.</p>
			<span class="sep"> | </span>
			<a class="arriba grey-text text-lighten-4 right" href="#legacy-top">Volver arriba</a>
		</div>
	</div>
</footer>
<?php wp_footer();?>
</div><!-- #page -->
</body>
</html>
