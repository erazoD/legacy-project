<?php
/**
 * Template Name: Quienes Somos
 *
 * @package WordPress
 * @subpackage legacy_theme
 */

get_header(); ?>

<div id="secondary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="titulo-seccion">
			<div class="banner-quien col s12" style="background:url(<?php echo get_field('who_banner_one')['url'];?>);">
				<div class="info-texto">
					<h4 class='fuente-b'><?php echo get_field('who_banner_title');?></h4>
				</div>
			</div>   
		</div>
		<div class="quienes-a row fondo">
		<div class="flex-b">
			<div class="quienes-somos col s12 m6 l6">
				<h5 class="fuente-a"><?php echo get_field('who_title_one');?></h5>
				<p class="fuente-a"><?php echo get_field('who_text_one');?></p>
			</div>
			<div class="quienes_img col s12 m6 l6">
				<img src="<?php echo get_field("who_image_one")['url'];?>" alt="somos-a">
			</div>
			</div>
		
			<div class="quienes_img col s12 m6 l6">
				<img src="<?php echo get_field("who_image_two")['url'];?>" alt="somos-a">
			</div>
			<div class="quienes-somos col s12 m6 l6">
				<h5 class="fuente-a"><?php echo get_field('who_title_two');?></h5>
				<p class="fuente-a"><?php echo get_field('who_text_two');?></p> 
			</div> 
		</div>
			<?php get_template_part('menu-somos');?> 
	</main><!-- #main -->
</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
