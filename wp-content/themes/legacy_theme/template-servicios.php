 
<?php
/**
 * Template Name: Servicios
 *
 * @package WordPress
 * @subpackage legacy_theme
 */

get_header(); ?>

<div id="secondary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="titulo-seccion">
			<div class="banner-quien col s12" style="background:url(<?php echo get_field('service_banner')['url'];?>);">
				<div class="info-texto">
					<h4 class='fuente-b'><?php echo get_field('service_banner_text');?></h4>
				</div>
			</div>   
		</div>
		<div class="texto-imperativo col s12">
		<img class='logo-servicio' src="<?php echo get_field('service_logo')['url'];?>" alt="">
			<div class="imperativo">
				<div class="container flex-b">
					<img src="<?php echo get_field('service_image_one')['url'];?>" alt="">
					<h5>Ofrecemos servicio personalizado en todo lo que se refiere a protección de riesgo y patrimonio.</h5>
				</div>
			</div>
			<div class="imperativo">
				<div class="container flex-b">
					<img src="<?php echo get_field('service_image_two')['url'];?>" alt="">
					<h5>No representamos ninguna compañía en particular, representamos al cliente frente a las más importantes aseguradoras en el mundo  para encontrar soluciones que se adapten a sus necesidades.</h5>
				</div>
			</div>
			<div class="imperativo">
				<div class="container flex-b">
					<img src="<?php echo get_field('service_image_three')['url'];?>" alt="">
					<h5>Analizamos su situación actual en productos de riesgo personal y lo asesoramos en la mejor solución sin ningún compromiso.</h5>
				</div>
			</div>
			<div class="imperativo">
				<div class="container flex-b">
					<img src="<?php echo get_field('service_image_four')['url'];?>" alt="">
					<h5>Nos comprometemos a brindar un completo acompañamiento a través de la vida útil de la solución para estar atentos a la dinámica que requiere cada uno de nuestros clientes.</h5>
				</div>
			</div>
			<div class="imperativo">
				<div class="container flex-b">
					<img src="<?php echo get_field('service_image_five')['url'];?>" alt="">
					<h5>
						Nos ajustamos al marco legal y a la normatividad de cada país.</h5>
					</div>
				</div>
			</div>
			<?php get_template_part('menu-somos');?> 
		</main><!-- #main -->
	</div><!-- #primary -->
	<?php
// get_sidebar();
	get_footer();



