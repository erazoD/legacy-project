<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package legacy_theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<title><?php echo wp_title(); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link   type='png' href="<?php bloginfo('template_url');?>/images/favicon.png">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C700%2C300italic%2C400italic%2C700italic%7CBitter%3A400%2C700&subset=latin%2Clatin-ext">
	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<header id="masthead" class="site-header" role="banner">
			<div class="site-branding">
			<a id='legacy-top' href="/"><img  class='legacy-logo' src="<?php bloginfo('template_url');?>/images/legacy-logo-hd.png"></a>	                                       
			</div><!-- .site-branding -->
			<div class="row navega">
				<div class="col s12 m12 l12">
					<nav class='navegador black'>
						<div class="nav-wrapper">
							<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>

							<?php if(!wp_is_mobile()){
								wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); 
							}else{
								wp_nav_menu( array( 'theme_location' => 'mobile-menu', 'menu_id' => 'mobile-demo',  'menu_class'=>'side-nav') );
							}?>
						</div>
					</nav>
				</div>
			</div>
		</header><!-- #masthead -->


		<div id="content" class="site-content">
